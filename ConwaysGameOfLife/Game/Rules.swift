//
//  Rules.swift
//  ConwaysGameOfLife
//
//  Created by TribalScale on 2021-05-14.
//

import Foundation

extension Array where Element == GameCell {
    var liveCellCount: Int { self.filter({ $0.isAlive }).count }
}

protocol GameRule {
    func apply(to cell: GameCell, with neighbours: [GameCell]) -> GameCell
}

struct UnderpopulationRule: GameRule {
    
    func apply(to cell: GameCell, with neighbours: [GameCell]) -> GameCell {
        if cell.isAlive && neighbours.liveCellCount < 2 {
            return GameCell(isAlive: false)
        }
        return cell
    }
}

struct NextGenerationRule: GameRule {
    
    func apply(to cell: GameCell, with neighbours: [GameCell]) -> GameCell {
        if cell.isAlive && (neighbours.liveCellCount == 2 || neighbours.liveCellCount == 3) {
            return GameCell(isAlive: true)
        }
        return cell
    }
}

struct OverpopulationRule: GameRule {
    
    func apply(to cell: GameCell, with neighbours: [GameCell]) -> GameCell {
        if cell.isAlive && neighbours.liveCellCount > 3 {
            return GameCell(isAlive: false)
        }
        return cell
    }
}

struct ReproductionRule: GameRule {
    
    func apply(to cell: GameCell, with neighbours: [GameCell]) -> GameCell {
        if !cell.isAlive && neighbours.liveCellCount == 3 {
            return GameCell(isAlive: true)
        }
        return cell
    }
}
