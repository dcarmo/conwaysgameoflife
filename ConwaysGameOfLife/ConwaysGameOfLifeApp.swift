//
//  ConwaysGameOfLifeApp.swift
//  ConwaysGameOfLife
//
//  Created by TribalScale on 2021-05-14.
//

import SwiftUI

@main
struct ConwaysGameOfLifeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
