//
//  ContentView.swift
//  ConwaysGameOfLife
//
//  Created by TribalScale on 2021-05-14.
//

import SwiftUI
import UIKit

let gridWidth = 10
let gridHeight = 20
let gridPadding: CGFloat = 10
let cellSize: CGFloat = (UIScreen.main.bounds.width - (2 * gridPadding)) / CGFloat(gridWidth)

struct ContentView: View {
    let columns = Array(repeating: GridItem(.fixed(cellSize), spacing: 0), count: gridWidth)
    
    var body: some View {
        LazyVGrid(columns: columns, spacing: 0) {
            ForEach(1...(gridWidth * gridHeight), id: \.self) { item in
                Rectangle()
                    .fill(Color.clear)
                    .aspectRatio(1, contentMode: .fit)
                    .border(Color.black)
            }
        }
        .aspectRatio(contentMode: .fit)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
