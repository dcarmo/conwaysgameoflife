//
//  RulesTests.swift
//  ConwaysGameOfLifeTests
//
//  Created by TribalScale on 2021-05-14.
//

import Foundation

import Mockingbird
import Nimble
import Quick

@testable import ConwaysGameOfLife

class RuleTests: QuickSpec {
    override func spec() {
        
        let possibleCases = [
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, false, false],
            [true, true, false, false, false, false, false, false],
            [true, true, true, false, false, false, false, false],
            [true, true, true, true, false, false, false, false],
            [true, true, true, true, true, false, false, false],
            [true, true, true, true, true, true, false, false],
            [true, true, true, true, true, true, true, false],
            [true, true, true, true, true, true, true, true]
        ]
        
        describe("an Underpopulation Rule") {
            
            var sut: UnderpopulationRule!
            
            beforeEach {
                sut = UnderpopulationRule()
            }
            
            describe("applying the rule to the cell") {
                
                context("when the cell is not alive") {
                    
                    var cell: GameCell!
                    beforeEach {
                        cell = GameCell(isAlive: false)
                    }
                    
                    let results = [false, false, false, false, false, false, false, false, false]
                    
                    possibleCases.enumerated().forEach { index, neighbourStates in
                        
                        context("when the cell has \(neighbourStates.filter({ $0 }).count) live neighbours") {
                            
                            it("should return a \(results[index] ? "live": "dead") cell") {
                                let nextCell = sut.apply(to: cell, with: neighbourStates.map(GameCell.init))
                                expect(nextCell.isAlive).to(results[index] ? beTrue() : beFalse())
                            }
                        }
                    }
                }
                
                context("when the cell is alive") {
                    var cell: GameCell!
                    beforeEach {
                        cell = GameCell(isAlive: true)
                    }
                    
                    let results = [false, false, true, true, true, true, true, true, true]
                    
                    possibleCases.enumerated().forEach { index, neighbourStates in
                        
                        context("when the cell has \(neighbourStates.filter({ $0 }).count) live neighbours") {
                            
                            it("should return a \(results[index] ? "live": "dead") cell") {
                                let nextCell = sut.apply(to: cell, with: neighbourStates.map(GameCell.init))
                                expect(nextCell.isAlive).to(results[index] ? beTrue() : beFalse())
                            }
                        }
                    }
                }
            }
        }
        
        describe("a Next Generation Rule") {
            
            var sut: NextGenerationRule!
            
            beforeEach {
                sut = NextGenerationRule()
            }
            
            describe("applying the rule to the cell") {
                
                context("when the cell is not alive") {
                    
                    var cell: GameCell!
                    beforeEach {
                        cell = GameCell(isAlive: false)
                    }
                    
                    let results = [false, false, false, false, false, false, false, false, false]
                    
                    possibleCases.enumerated().forEach { index, neighbourStates in
                        
                        context("when the cell has \(neighbourStates.filter({ $0 }).count) live neighbours") {
                            
                            it("should return a \(results[index] ? "live": "dead") cell") {
                                let nextCell = sut.apply(to: cell, with: neighbourStates.map(GameCell.init))
                                expect(nextCell.isAlive).to(results[index] ? beTrue() : beFalse())
                            }
                        }
                    }
                }
                
                context("when the cell is alive") {
                    var cell: GameCell!
                    beforeEach {
                        cell = GameCell(isAlive: true)
                    }
                    
                    let results = [true, true, true, true, true, true, true, true, true]
                    
                    possibleCases.enumerated().forEach { index, neighbourStates in
                        
                        context("when the cell has \(neighbourStates.filter({ $0 }).count) live neighbours") {
                            
                            it("should return a \(results[index] ? "live": "dead") cell") {
                                let nextCell = sut.apply(to: cell, with: neighbourStates.map(GameCell.init))
                                expect(nextCell.isAlive).to(results[index] ? beTrue() : beFalse())
                            }
                        }
                    }
                }
            }
        }
        
        describe("an Overpopulation Rule") {
            
            var sut: OverpopulationRule!
            
            beforeEach {
                sut = OverpopulationRule()
            }
            
            describe("applying the rule to the cell") {
                
                context("when the cell is not alive") {
                    
                    var cell: GameCell!
                    beforeEach {
                        cell = GameCell(isAlive: false)
                    }
                    
                    let results = [false, false, false, false, false, false, false, false, false]
                    
                    possibleCases.enumerated().forEach { index, neighbourStates in
                        
                        context("when the cell has \(neighbourStates.filter({ $0 }).count) live neighbours") {
                            
                            it("should return a \(results[index] ? "live": "dead") cell") {
                                let nextCell = sut.apply(to: cell, with: neighbourStates.map(GameCell.init))
                                expect(nextCell.isAlive).to(results[index] ? beTrue() : beFalse())
                            }
                        }
                    }
                }
                
                context("when the cell is alive") {
                    var cell: GameCell!
                    beforeEach {
                        cell = GameCell(isAlive: true)
                    }

                    let results = [true, true, true, true, false, false, false, false, false]

                    possibleCases.enumerated().forEach { index, neighbourStates in

                        context("when the cell has \(neighbourStates.filter({ $0 }).count) live neighbours") {

                            it("should return a \(results[index] ? "live": "dead") cell") {
                                let nextCell = sut.apply(to: cell, with: neighbourStates.map(GameCell.init))
                                expect(nextCell.isAlive).to(results[index] ? beTrue() : beFalse())
                            }
                        }
                    }
                }
            }
        }
        
        describe("a Reproduction Rule") {
            
            var sut: ReproductionRule!
            
            beforeEach {
                sut = ReproductionRule()
            }
            
            describe("applying the rule to the cell") {
                
                context("when the cell is not alive") {
                    
                    var cell: GameCell!
                    beforeEach {
                        cell = GameCell(isAlive: false)
                    }
                    
                    let results = [false, false, false, true, false, false, false, false, false]
                    
                    possibleCases.enumerated().forEach { index, neighbourStates in
                        
                        context("when the cell has \(neighbourStates.filter({ $0 }).count) live neighbours") {
                            
                            it("should return a \(results[index] ? "live": "dead") cell") {
                                let nextCell = sut.apply(to: cell, with: neighbourStates.map(GameCell.init))
                                expect(nextCell.isAlive).to(results[index] ? beTrue() : beFalse())
                            }
                        }
                    }
                }
                
                context("when the cell is alive") {
                    var cell: GameCell!
                    beforeEach {
                        cell = GameCell(isAlive: true)
                    }

                    let results = [true, true, true, true, true, true, true, true, true]

                    possibleCases.enumerated().forEach { index, neighbourStates in

                        context("when the cell has \(neighbourStates.filter({ $0 }).count) live neighbours") {

                            it("should return a \(results[index] ? "live": "dead") cell") {
                                let nextCell = sut.apply(to: cell, with: neighbourStates.map(GameCell.init))
                                expect(nextCell.isAlive).to(results[index] ? beTrue() : beFalse())
                            }
                        }
                    }
                }
            }
        }
    }
}

